<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\PublicAsset;
use yii\helpers\Html;
use yii\helpers\Url;

PublicAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link id="favicon" rel="shortcut icon" href="/public/images/favicon.png" type="image/png" />
	<link rel="stylesheet" href="node_modules/@glidejs/glide/dist/css/glide.theme.min.css">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
	<script src="https://api-maps.yandex.ru/2.1/?apikey=bd742b2e-6583-4879-8110-7120cd95fbe5&lang=ru_RU" type="text/javascript"></script>
</head>
<body>
<?php $this->beginBody() ?>
	
<style>
	.footer-mob__logo {
		display: none;
	}
	.footer-mob__logo img {
		width: 70%;
	}
	@media (max-width: 480px) {
		.footer__logo {
			display: none;
		}
		.footer-mob__logo {
			display: block;
		}
		.footer .fab {
			color: #f68e52;
			border: 1px solid #2c333f;
		}
	}
</style>

<!-- HEADER -->
<header class="header" id="header">
    <div class="header-row">
        <div class="container">
            <a href="/" class="logo">
                <img src="<?=Yii::$app->view->params['logo']->getImage();?>">
            </a>
            <div class="header-navigation">
                <ul>
                    <? foreach ( Yii::$app->view->params['menu'] as $v):?>
                    <li><a href="/site/index<?=$v->url?>"><?=$v->text?></a></li>
                    <? endforeach;?>
                </ul>
            </div>
			<div >
				<div class="row">
				<div class="header-navigation-lang">
					<a href="/lang/index?url=ru" class="<?=(Yii::$app->session["lang"]=='')?'active':''?>">RU</a>
				
					<a href="/lang/index?url=kz" class="<?=(Yii::$app->session["lang"]=='_kz')?'active':''?>">KAZ</a>

					<a  href="/lang/index?url=en" class="<?=(Yii::$app->session["lang"]=='_en')?'active':''?>">ENG</a>
				</div>
					</div>
				
			</div>
        </div>
    </div>

    <div class="header-content wow fadeInLeft" data-wow-duration="1s">
        <div class="container">
            <h2 id="h2_mobile"><?=Yii::$app->view->params['banner']->name?></h2>
            <p><?=Yii::$app->view->params['banner']->subname?></p>
			<a href="<?=Yii::$app->view->params['banner']->url?>" class="btn-more">
					<?=Yii::$app->view->params['banner']->buttonName?>
				 </a>
			<div class="btn-more-wrap">
				 
            </div>
           
            <span><?=Yii::$app->view->params['banner']->content?></span>
        </div>
    </div>

    <a class="header-menu-toggle" href="#0">
        <span class="header-menu-icon"></span>
        <span class="header-menu-icon mt-2"></span>
        <span class="header-menu-icon mt-2"></span>
    </a>

    <!-- MOBILE MENU -->
    <nav class="header-nav shadow">

        <a href="#0" class="header-nav__close" title="close"><span>Close</span></a>

        <div class="header-nav__content">
            <ul>
                <? $class = 'active';?>
                <? foreach ( Yii::$app->view->params['menu'] as $k => $v):?>
				<?
					$mob = '';
					if($k == 0) {
						$mob = '-mob-anchor';
					}
				    if ($k == 2){
						$mob = "-mob";}
				?>
                    <li><a href="/site/index/<?=$v->url.$mob?>" class="<?=$class;?>"><?=$v->text?></a></li>
                <? $active = "";?>
                <? endforeach;?>
				<div class="header-navigation-lang_mobi">
					<a href="/lang/index?url=ru" class="<?=(Yii::$app->session["lang"]=='')?'active':''?>">RU</a>
				
				<a href="/lang/index?url=kz" class="<?=(Yii::$app->session["lang"]=='_kz')?'active':''?>">KAZ</a>
				
				<a  href="/lang/index?url=en" class="<?=(Yii::$app->session["lang"]=='_en')?'active':''?>">ENG</a>
				</div>
				
            </ul>
			
        </div>
    </nav>
    <!-- END MOBILE MENU -->
</header>
<!-- END HEADER -->

<?=$content;?>

<!-- FOOTER -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="row justify-content-between">
                    <div class="footer__logo col-sm-12 col-md-6 col-lg-4">
                        <img src="<?=Yii::$app->view->params['logo']->getImage();?>" alt="">
                    </div>
					<div class="footer-mob__logo col-sm-12 col-md-6 col-lg-4">
                        <img src="/public/images/footer-mob-logo.png" alt="">
                    </div>
                    <div class="vertical-center col-sm-12 col-md-6 col-lg-4">
                        <div class="row social-net col-sm-12 col-md-6 col-lg-4">
                            <div class="col-sm-4 col-md-4 col-lg-4" id="fab">
                                <a href="<?=Yii::$app->view->params['socialNetwork']->facebook?>" target="_blank">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4" id="fab">
                                <a href="<?=Yii::$app->view->params['socialNetwork']->instagram?>" target="_blank">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </div>
                            <!--<div class="col-sm-4 col-md-4 col-lg-4" id="fab">
                                <a href="<?=Yii::$app->view->params['socialNetwork']->twitter?>" target="_blank">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </div> -->
                        </div>
                    </div>
                    <div class="vertical-center col-sm-12 col-md-6 col-lg-4">
                        <div class="footer__madeby">
                            <p>Разработано в <a href="https://a-lux.kz" target="_blank">ALUX</a></p>
                        </div>
                    </div>
                    <div class="vertical-center col-sm-12 col-md-6 col-lg-4">
                        <div class="footer__right-corner">
                            <p class="footer--grey"><?=Yii::$app->view->params['logo']->copyright;?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- END FOOTER -->
<script type="text/javascript">
				$(document).on('ready', function() {
					$(".slider-main").slick({
						arrows: true,
						dots: false,
						adaptiveHeight: true,
						slidesToShow: 1
					});
					$(".slider-submain").slick({
						arrows: true,
						dots: true,
						adaptiveHeight: true ,
						slidesToShow: 1,
						draggable: false
					});
				});
			</script>
	<!--<script src="https://cdn.jsdelivr.net/npm/@glidejs/glide"></script>
	<script src="https://unpkg.com/@glidejs/glide"></script>-->
	<script src="https://unpkg.com/micromodal/dist/micromodal.min.js">
	
<?php $this->endBody();?>
</body>
</html>
<?php $this->endPage() ?>
