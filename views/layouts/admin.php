<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AdminAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

AdminAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
	 <script src="https://api-maps.yandex.ru/2.1/?apikey=bd742b2e-6583-4879-8110-7120cd95fbe5&lang=ru_RU" type="text/javascript"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>

<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="<?= Url::toRoute(['menu/index'])?>" class="logo">
            <span class="logo-mini"></span>
            <span class="logo-lg">Админ. Панель</span>
        </a>
<!--        --><?//= Html::a('<span class="logo-mini"></span><span class="logo-lg">Админ. панель</span>', ['default/index'], ['class' => 'logo']) ?>
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <?= Html::a(
                            'Выйти',
                            ['/auth/logout']
                        ) ?>
                    </li>

                    <select class="language-button selectpicker" onchange="location = this.options[this.selectedIndex].value;">
                        <option <?=(Yii::$app->session["lang"]=='')?'selected':''?> value="/lang/index?url=ru">RU</option>
                        <option <?=(Yii::$app->session["lang"]=='_kz')?'selected':''?> value="/lang/index?url=kz">KZ</option>
                        <option <?=(Yii::$app->session["lang"]=='_en')?'selected':''?> value="/lang/index?url=en">EN</option>
                    </select>

                </ul>
            </div>
        </nav>
    </header>

    <aside class="main-sidebar">
        <section class="sidebar">
            <?= dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                    'items' => [
                        ['label' => 'Переводы', 'icon' => 'fa fa-user', 'url' => ['/admin/translation/index'],'active' => $this->context->id == 'translation'],
                        ['label' => 'Страницы', 'icon' => 'fa fa-user', 'url' => ['/admin/page/index'],'active' => $this->context->id == 'page'],
                        [
                            'label' => 'Шапка',
                            'icon' => 'fa fa-user',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Меню', 'icon' => 'fa fa-user', 'url' => ['/admin/menu/index'],'active' => $this->context->id == 'menu'],
                                ['label' => 'Баннер', 'icon' => 'fa fa-user', 'url' => ['/admin/banner/index'],'active' => $this->context->id == 'banner'],

                            ],
                        ],
                        [
                            'label' => 'Главная страница',
                            'icon' => 'fa fa-user',
                            'url' => '#',
                            'items' => [
                                [
                                    'label' => 'Кредитные карты',
                                    'icon' => 'fa fa-user',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Задний фон', 'icon' => 'fa fa-user', 'url' => ['/admin/creditcardsub/index'],'active' => $this->context->id == 'creditcardsub'],
                                        ['label' => 'Кредитные карты', 'icon' => 'fa fa-user', 'url' => ['/admin/creditcard/index'],'active' => $this->context->id == 'creditcard'],
                                        ['label' => 'Содержание', 'icon' => 'fa fa-user', 'url' => ['/admin/creditcardtype/index'], 'active' => $this->context->id == 'creditcardtype'],
                                    ],
                                ],

                                ['label' => 'Акции и скидки', 'icon' => 'fa fa-user', 'url' => ['/admin/promosub/index'],'active' => $this->context->id == 'promosub'],
                                ['label' => 'Новости', 'icon' => 'fa fa-user', 'url' => ['/admin/newssub/index'],'active' => $this->context->id == 'newssub'],
                                ['label' => 'Форма для заявка', 'icon' => 'fa fa-user', 'url' => ['/admin/requestsub/index'],'active' => $this->context->id == 'requestsub'],
                                ['label' => 'Контакты', 'icon' => 'fa fa-user', 'url' => ['/admin/contactsub/index'],'active' => $this->context->id == 'contactsub'],
                            ],
                        ],

                        ['label' => 'Новости', 'icon' => 'fa fa-user', 'url' => ['/admin/news/index'],'active' => $this->context->id == 'news'],

                        ['label' => 'Акции и скидки', 'icon' => 'fa fa-user', 'url' => ['/admin/promotion/index'],'active' => $this->context->id == 'promotion'],

                        [
                            'label' => 'Контакты',
                            'icon' => 'fa fa-user',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Города', 'icon' => 'fa fa-user', 'url' => ['/admin/city/index'],'active' => $this->context->id == 'city'],
                                ['label' => 'Рабочее время', 'icon' => 'fa fa-user', 'url' => ['/admin/worktime/index'],'active' => $this->context->id == 'worktime'],
                                ['label' => 'Контакты', 'icon' => 'fa fa-user', 'url' => ['/admin/contact/index'],'active' => $this->context->id == 'contact'],
                                ['label' => 'Cоциальные сети', 'icon' => 'fa fa-user', 'url' => ['/admin/socialnetwork/index'],'active' => $this->context->id == 'socialnetwork'],

                            ],
                        ],
                        ['label' => 'Эл. почта для связи', 'icon' => 'fa fa-user', 'url' => ['/admin/emailforrequest/index'],'active' => $this->context->id == 'emailforrequest'],
                        ['label' => 'Заявки', 'icon' => 'fa fa-user', 'url' => ['/admin/request/index'],'active' => $this->context->id == 'request'],
                        ['label' => 'Согласование', 'icon' => 'fa fa-user', 'url' => ['/admin/agreement/index'],  'active' => $this->context->id == 'agreement'],
                        ['label' => 'Логотип и Копирайт', 'icon' => 'fa fa-user', 'url' => ['/admin/logo/index'],'active' => $this->context->id == 'logo'],

                    ]
                ]
            ) ?>
        </section>
    </aside>



    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>

            <?=$content?>

        </section>
    </div>

    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.4.0
        </div>
        <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
        reserved.
    </footer>


    <div class="control-sidebar-bg"></div>
</div>

<?php $this->endBody() ?>
<?php $this->registerJsFile('/ckeditor/ckeditor.js');?>
<?php $this->registerJsFile('/ckfinder/ckfinder.js');?>
<script>
    $(document).ready(function(){
        var editor = CKEDITOR.replaceAll();
        CKFinder.setupCKEditor( editor );
    })
    $.widget.bridge('uibutton', $.ui.button);
</script>
</body>
</html>
<?php $this->endPage() ?>
