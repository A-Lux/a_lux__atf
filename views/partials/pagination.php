<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 01.03.2019
 * Time: 17:54
 */

use yii\widgets\LinkPager;

?>

<div class="atf-pagination">
    <?php
    echo LinkPager::widget([
        'pagination' => $pagination,
        'prevPageCssClass' => 'prev-page a',
        'nextPageCssClass' => 'nxt-page',
        'activePageCssClass' => 'active' ,
        'hideOnSinglePage' => true,
    ]);
    ?>
</div>
