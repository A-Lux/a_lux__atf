<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 22.02.2019
 * Time: 11:48
 */

use app\controllers\ContentController;
use yii\helpers\Url;

Yii::$app->view->params['title'] = 'Поиск';
Yii::$app->view->params['desc'] = 'Поиск';
Yii::$app->view->params['key'] = 'Поиск';

?>

<main>
    <div class="inner-main">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="bread-crumbs wow fadeInLeft">
                        <ul>
                            <li><a href="<?=Url::toRoute([Yii::$app->view->params['menu'][0]->url])?>">Главная</a></li>
                            <li>/</li>
                            <li>ПОИСК</li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-9 ">
                    <?if($count){?>
                        <div class="" style="font-size: 22px;margin-bottom: 20px;">Результаты поиска по запросу <span style="font-weight: bold;"><?=$search?></span>.</div>
                    <?}else{?>
                        <div class="" style="font-size: 22px;margin-bottom: 20px;">По запросу <span style="font-weight: bold;"><?=$search?></span> ничего не найдено.</div>
                    <?}?>

                    <? if($about != null):?>
                        <div class="wow flipInY inner-title">
                            <h3><?=Yii::$app->view->params['submenu'][0]->name?></h3>
                        </div>
                        <div class="text-block wow slideInLeft"  style="margin-top: 50px;">
                            <? $k=0;$m=0;?>
                            <? foreach ($about as $v):?>
                                <? $m++?>
                                <? $content = $v->aboutItems;?>
                                <? if($k == 0):?>
                                    <div class="inner-title wow slideInLeft">
                                        <h4><?=$v->name?></h4>
                                    </div>
                                    <? foreach ($content as $con):?>
                                        <p><img src="/public/images/circle.png" alt=""><?=\app\controllers\ContentController::cutStr($con->content,150)?>
                                            <a href="<?=URL::toRoute(['site/about'])?>" style="margin-left: 10px;color: #007bff;"  >Читать далее</a></p>

                                    <? endforeach;?>
                                    <? $k=1;?>
                                    <? continue;?>
                                <? endif;?>

                                <? if($v->image == null):?>
                                    <div class="inner-title space-top">
                                        <h4><?=$v->name?></h4>
                                    </div>

                                    <? foreach ($content as $con):?>
                                        <p><img src="/public/images/circle.png" alt=""><?=\app\controllers\ContentController::cutStr($con->content,150)?>
                                            <a href="<?=URL::toRoute(['site/about'])?>" style="margin-left: 10px;color: #007bff;"  >Читать далее</a></p>

                                    <? endforeach;?>
                                <? endif;?>

                                <? if($v->image != null):?>
                                    <? if($v->position == 0):?>
                                        <div class="left-img">
                                            <img src="<?=$v->getImage();?>" class="img-fluid" alt="">
                                        </div>
                                        <div class="inner-title">
                                            <h4><?=$v->name?></h4>
                                        </div>
                                        <? foreach ($content as $con):?>
                                            <p><img src="/public/images/circle.png" alt=""><?=\app\controllers\ContentController::cutStr($con->content,150)?>
                                                <a href="<?=URL::toRoute(['site/about'])?>" style="margin-left: 10px;color: #007bff;"  >Читать далее</a></p>

                                        <? endforeach;?>
                                    <? endif;?>
                                <? endif;?>

                            <? endforeach;?>

                        </div>
                        <div class="row">
                            <? $m=0;?>
                            <? foreach ($about as $v):?>
                                <? $m++?>
                                <? if($v->position == 1):?>
                                    <div class="col-sm-7 wow fadeInUp">
                                        <div class="inner-title ">
                                            <h4><?=$v->name?></h4>
                                        </div>
                                        <div class="text-block">
                                            <? foreach ($content as $con):?>
                                                <p><img src="/public/images/circle.png" alt=""><?=\app\controllers\ContentController::cutStr($con->content,150)?>
                                                    <a href="<?=URL::toRoute(['site/about'])?>" style="margin-left: 10px;color: #007bff;"  >Читать далее</a></p>

                                            <? endforeach;?>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="right-img wow slideInRight">
                                            <img src="/public/images/about-img2.png" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                <? endif;?>
                            <? endforeach;?>
                        </div>
                    <? endif;?>

                    <? if($news != null):?>
                        <div class="inner-title wow flipInX">
                            <h3><?=Yii::$app->view->params['submenu'][1]->name?></h3>
                        </div>
                        <? foreach ($news as $v):?>
                            <div class="inner-news-item wow slideInLeft">
                                <a href="<?=URL::toRoute(['site/news-view','id'=>$v->id])?>">
                                    <div class="news-item-date">
                                        <p><?=$v->getDate();?></p>
                                    </div>
                                    <div class="news-title">
                                        <p><?=$v->name?></p>

                                    </div>
                                    <div class="news-arrow">
                                        <img src="/public/images/news-arrow.png" alt="">
                                    </div>
                                </a>
                            </div>
                        <? endforeach;?>
                    <? endif;?>


                    <? if($article != null):?>
                        <div class="inner-title wow flipInX">
                            <h3><?=Yii::$app->view->params['submenu'][2]->name?></h3>
                        </div>
                        <? foreach ($article as $v):?>
                            <div class="inner-news-item wow slideInLeft">
                                <a href="<?=URL::toRoute(['site/article-view','id'=>$v->id])?>">
                                    <div class="news-item-date">
                                        <p><?=$v->getDate();?></p>
                                    </div>
                                    <div class="news-title">
                                        <p><?=$v->name?></p>

                                    </div>
                                    <div class="news-arrow">
                                        <img src="/public/images/news-arrow.png" alt="">
                                    </div>
                                </a>
                            </div>
                        <? endforeach;?>

                    <? endif;?>

                    <? if($factory != null):?>
                        <div class="inner-title wow flipInX">
                            <h3><?=Yii::$app->view->params['submenu'][3]->name?></h3>
                        </div>

                        <?=\app\controllers\ContentController::cutStr($factory->content,500);?>
                        <a href="<?=URL::toRoute(['site/factory'])?>" style="color: #007bff;"  >Читать далее</a></div>

                    <? endif;?>

                    <? if($innovation != null):?>
                        <div class="inner-title wow flipInX">
                            <h3><?=Yii::$app->view->params['submenu'][4]->name?></h3>
                        </div>
                        <img src="<?=$innovation->getImage();?>" class="left-img img-fluid" alt="">
                        <p style="margin-bottom: 100px"><?=\app\controllers\ContentController::cutStr($innovation->content,800)?>
                            <a href="<?=URL::toRoute(['site/innovation'])?>" style="margin-left: 10px;color: #007bff;"  >Читать далее</a></p>
                    <? endif;?>

                    <? if($valuetype != null):?>
                        <div class="inner-title wow flipInX">
                            <h3><?=Yii::$app->view->params['submenu'][6]->name?></h3>
                        </div>
                        <? foreach ($valuetype as $v):?>
                        <div class="inner-title wow slideInUp">
                            <h4><?=$v->name?></h4>
                        </div>
                        <div class="text-block  wow slideInUp">
                            <p><?=\app\controllers\ContentController::cutStr($v->content,300)?>
                                <a href="<?=URL::toRoute(['site/values'])?>" style="margin-left: 10px;color: #007bff;"  >Читать далее</a></p>
                        </div>
                        <? endforeach;?>
                    <? endif;?>

                    <? if($social != null):?>
                        <div class="inner-title wow flipInX">
                            <h3><?=Yii::$app->view->params['submenu'][7]->name?></h3>
                        </div>

                        <?=\app\controllers\ContentController::cutStr($social->contenta,500)?>
                        <a href="<?=URL::toRoute(['site/social-responsibility'])?>" style="color: #007bff;"  >Читать далее</a></div>
                    <? endif;?>

                    <? if($category != null):?>
                        <div class="inner-title wow flipInX">
                            <h3><?=Yii::$app->view->params['menu'][1]->text?></h3>
                        </div>
                        <? foreach ($category as $v):?>
                            <div class="product-title">
                                <a href="<?=Url::toRoute(['site/catalog','id'=>$v->id])?>">
                                    <div class="product-icon">
                                        <img src="<?=$v->getImagea();?>" alt="">
                                    </div>
                                    <p><?=$v->name?></p>
                                </a>
                            </div>
                            <p><?=\app\controllers\ContentController::cutStr($v->content,300)?>
                                <a href="<?=URL::toRoute(['site/category'])?>" style="color: #007bff;">Читать далее</a></p>
                        <?endforeach;?>
                    <? endif;?>

                    <? if($product != null):?>
                        <div class="inner-title wow flipInY" style="margin-top: 50px;">
                            <h3>Продукты</h3>
                        </div>
                        <div class="row">
                            <? foreach ($product as $v):?>
                                    <div class="col-lg-4">
                                        <div class="catalog-item wow slideInUp">
                                            <a href="<?=Url::toRoute(['site/product-view','id'=>$v->id])?>">
                                                <img src="<?=$v->getImageA();?>" alt="">
                                                <div class="catalog-title">
                                                    <h6><?=$v->name?></h6>
                                                </div>
                                                <div class="catalog-text">
                                                    <p><?=$v->subcontent?></p>
                                                </div>
                                                <a href="<?=Url::toRoute(['site/product-view','id'=>$v->id])?>" class="btn btn-catalog btn-more">Узнать больше</a>
                                            </a>
                                        </div>
                                    </div>
                            <? endforeach;?>
                        </div>
                    <? endif;?>
                </div>
                <?=$this->render('/partials/banner');?>
            </div>

        </div>

    </div>

</main>






