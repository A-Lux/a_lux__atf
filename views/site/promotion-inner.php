<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 15.04.2019
 * Time: 10:44
 */
?>

<main class="main">
    <!-- ARCHIVE NEWS -->
    <div id="promotion-view" class="inner-news">
        <div class="container">
            <!-- хлебные крошки -->
            <div class="bread-crumbs">
                <ul>
                    <li><a href="<?=Yii::$app->view->params['main']->url?>"><?=Yii::$app->view->params['main']->text;?></a></li>
                    <li><a href="<?=$model->url;?>"><?=$model->text;?></a></li>
                    <li><a href="/site/promotion-view?id="<?= $promotion->id?>><?= $promotion->name?></a></li>
                </ul>
            </div>
            <!-- end хлебные крошки -->

            <div class="inner-news-img">
                <img src="<?=$promotion->getImageb()?>">
            </div>

            <div class="inner-news-title">
                <h3><?= $promotion->name?></h3>
                <span><?= $promotion->getDate();?></span>
                <?= $promotion->content?>
            </div>
        </div>
    </div>

    <div class="arch-news-wrapper-pink">
        <div class="container">
            <div class="title">
                <h3><?=Yii::$app->view->params['translation'][18]->name;?></h3>
            </div>
            <div class="inner-news-pink-wrapper">
                <? foreach ($recomend as $v):?>
                    <div class="news-card">
                        <img src="<?=$v->getImagea();?>">
                        <div class="news_content">
                            <h4><?=$v->type;?></h4>
							<h3><a href="/site/promotion-view?id=<?=$v->id?>"><?=$v->name;?></a></h3>
                            <p><?=$v->subcontent;?></p>
                            <span><?=$v->getDate();?></span>
                        </div>
                    </div>
                <? endforeach;?>

            </div>
        </div>
    </div>
    <!-- END ARCHIVE NEWS -->
</main>
