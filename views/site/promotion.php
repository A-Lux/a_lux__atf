<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 15.04.2019
 * Time: 10:44
 */

?>

<main class="main">
    <!-- PROMOTIONS -->
    <div id="promotions-block" class="promotions">
        <div class="container">
            <!-- хлебные крошки -->
            <div class="bread-crumbs">
                <ul>
                    <li><a href="<?=Yii::$app->view->params['main']->url?>"><?=Yii::$app->view->params['main']->text;?></a></li>
                    <li><a><?=$model->text;?></a></li>
                </ul>
            </div>
            <!-- end хлебные крошки -->
            <div class="title">
                <h3><?=$model->text;?></h3>
            </div>

            <div class="promotions-wrapper">
                <? foreach ($promotion['data'] as $v):?>
                <div class="promotions-card">
                    <img src="<?=$v->getImageA()?>">
                    <div class="news_content">
                        <h4><?=$v->type?></h4>
						<h3><a href="/site/promotion-view?id=<?=$v->id?>"><?=$v->name;?></a></h3>
                        <p><?=$v->subcontent?></p>
                        <span><?=$v->getDate();?></span>
                    </div>
                </div>
                <? endforeach;?>
            </div>

            <?= $this->render('/partials/pagination', [
                'pagination'=>$promotion['pagination'],
            ]);?>

        </div>
    </div>
    <!-- END PROMOTIONS -->
</main>
