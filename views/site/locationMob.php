<? $m=0;?>
<? foreach ($data as $v):?>
    <? $m++;?>
    <div class="adress__locations__items">
        <div class="row">
            <div class="col-sm-5 col-md-4 col-lg-1">
                <div class="address__locations__items__circle--white">
                    <h4> <?=$m;?></h4>
                </div>
                <div class="address__locations__items__circle__mobile--white">
                    <i class="fas fa-map-marker-alt"></i>
                    <h4> <?=$v->address?></h4>
                </div>
            </div>
            <div class="col-sm-7 col-md-8 col-lg-11">
                <div class="mob-version__address">
                  
                    <p>   <?=$v->workTime?>
                        <?=$v->telephone?> </p>
                </div>
            </div>
        </div>
    </div>
<? endforeach;?>