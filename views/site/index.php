<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

?>

<main class="main">
    <style>
        .premium-package .premium-bottom .left-side ul li span img,
        .premium-package .premium-bottom .right-side ul li span img {
            width: auto !important;
            display: inline !important;
        }

        .owl-prev,
        .owl-next {
            position: absolute;
            top: 45%;
        }

        .owl-prev {
            left: 15px;
        }

        .owl-next {
            right: 15px;
        }

        .ornament1 {
            z-index: 1;
            display: none;
            position: absolute;
            z-index: -1;
            top: -78px;
            left: 64px;
            transform: rotate(90deg);
        }

        .ornament1 img {
            width: 220px !important;
        }

        .ornament2 {
            position: absolute;
            right: 0;
            z-index: -1;
            bottom: 0;
            transform: rotate(180deg);
            display: none;
        }

        .ornament2 img {
            width: 220px !important;
        }

        .ornament3 {
            position: absolute;
            right: 0;
            z-index: 0;
            top: -65px;
            right: 0;
            display: none;
        }

        .ornament3 img {
            width: 260px !important;
        }

        .block-questions {
            position: relative;
            overflow: hidden;
        }

        .ornament4 {
            position: absolute;
            left: 0;
            transform: rotate(180deg);
            z-index: 0;
            top: 0;
            display: none;
        }

        .ornament4 img {
            width: 260px !important;
        }

        @media (max-width: 480px) {

            .ornament1,
            .ornament2,
            .ornament3,
            .ornament4 {
                display: block;
            }
        }
    </style>
    <!-- PREMIUM-PACKAGE -->


	<script>

	

		$('body').on('click', ".btn-send-request-card", function () {
			var name = $(".feedbackName_card").val();
			var phone = $(".feedbackPhone_card").val();
			var city = $(".feedbackCity_card").val();
			var hour = $(".feedbackHour_card").val();
			var minute = $(".feedbackMinute_card").val();
			var day = $(".feedbackDay_card").val();
			var chekbox = $("#checkbox").prop('checked');
			var card = $('.feedbackCard_card').val();


			if (!name) {
				swal('<?=Yii::$app->view->params['translation'][16]->name;?>!', '<?=Yii::$app->view->params['translation'][11]->name;?>', 'error');
			}else if (!phone) {
				swal('<?=Yii::$app->view->params['translation'][16]->name;?>!', '<?=Yii::$app->view->params['translation'][12]->name;?>', 'error');
			} else if (!city) {
				swal('<?=Yii::$app->view->params['translation'][16]->name;?>!', '<?=Yii::$app->view->params['translation'][13]->name;?>', 'error');
			}else if(!chekbox){
				swal('Ошибка!', 'Подвердите!', 'error');
			}else {
				swal('<?=Yii::$app->view->params['translation'][14]->name;?>', '<?=Yii::$app->view->params['translation'][15]->name;?>', 'success');
				$.ajax({
					type: "GET",
					dataType: "json",
					url: "/site/request-from-card",
					data: { name: name, phone: phone, city: city, hour: hour, minute: minute, day: day, card:card},
					success: function (data) {
						if (data.status) {
							
						}else {
							swal('Упс!', 'Что-то пошло не так.', 'error');
						}
					},
					error: function () {
						swal('Упс!', 'Что-то пошло не так.', 'error');
					}
				});

				$.ajax({
					type: "GET",
					dataType: "json",
					url: "/site/request-email",
					data: { name: name, phone: phone, city: city, hour: hour, minute: minute, day: day },
					success: function (data) {

					},
					error: function () {
						swal('Упс!', 'Что-то пошло не так.', 'error');
					}
				});
			}
		});


		$('body').on('click', ".btn-send-request-card-mob", function () {
			var name = $(".feedbackName_card_mob").val();
			var phone = $(".feedbackPhone_card_mob").val();
			var city = $(".feedbackCity_card_mob").val();
			var hour = $(".feedbackHour_card_mob").val();
			var minute = $(".feedbackMinute_card_mob").val();
			var day = $(".feedbackDay_card_mob").val();
			var chekbox = $("#checkbox_mob").prop('checked');
			var card = $('.feedbackCard_card_mob').val();


			if (!name) {
				swal('<?=Yii::$app->view->params['translation'][16]->name;?>!', '<?=Yii::$app->view->params['translation'][11]->name;?>', 'error');
			}else if (!phone) {
				swal('<?=Yii::$app->view->params['translation'][16]->name;?>!', '<?=Yii::$app->view->params['translation'][12]->name;?>', 'error');
			} else if (!city) {
				swal('<?=Yii::$app->view->params['translation'][16]->name;?>!', '<?=Yii::$app->view->params['translation'][13]->name;?>', 'error');
			}else if(!chekbox){
				swal('Ошибка!', 'Подвердите!', 'error');
			}else {
				swal('<?=Yii::$app->view->params['translation'][14]->name;?>', '<?=Yii::$app->view->params['translation'][15]->name;?>', 'success');
				$.ajax({
					type: "GET",
					dataType: "json",
					url: "/site/request-from-card",
					data: { name: name, phone: phone, city: city, hour: hour, minute: minute, day: day, card:card},
					success: function (data) {
						if (data.status) {
							
						}else {
							swal('Упс!', 'Что-то пошло не так.', 'error');
						}
					},
					error: function () {
						swal('Упс!', 'Что-то пошло не так.', 'error');
					}
				});

				$.ajax({
					type: "GET",
					dataType: "json",
					url: "/site/request-email",
					data: { name: name, phone: phone, city: city, hour: hour, minute: minute, day: day },
					success: function (data) {

					},
					error: function () {
						swal('Упс!', 'Что-то пошло не так.', 'error');
					}
				});
			}
		});
	</script>
    <div id="mainCarousel" class="owl-carousel owl-theme premium-package"
         style="padding-bottom: 2rem;background-position: center;background-repeat: no-repeat;-webkit-background-size: cover;background-size: cover;background-image: url(<?=$card['sub']->getImage();?>);">
        <? $class = 'active';?>
        <? foreach ($card['card'] as $k => $v):?>
		<? if($v->id != 1):?>
            <? $types = $v->cardItems;?>
            <div class="container">
                <span class="ornament1"><img src="/public/images/ornament.png"></span>
                <div class="premium-top">
                    <div class="premium-cards">
                        <img class="rotateInDownLeft img-fluid" src="<?=$v->getImage();?>" data-wow-delay="1.5s">
                        <!-- Button trigger modal -->


                        <div class="promo-video__parent">

                        <!--                        <span  class="feedback-video" data-toggle="modal">-->
<!--						<a class="promo-video" >-->
<!--                        --><?//=Yii::$app->view->params['translation'][0]->name;?>
<!--                        </a>-->
<!--                        </span>-->
<!---->
<!--                        <div class="submit-modal-video modal-window-submit-video">-->
<!--                            <div class="close-modal-video">-->
<!--                                <a href=""><i class="fas fa-times"></i></a>-->
<!--                            </div>-->
<!--                            <iframe width="789" height="444" src="--><?//=$v->url?><!--" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>-->
<!--                        </div>-->
                 
                        </div>
                    </div>

                    <div class="premium-services  fadeInRight" data-wow-delay="1.5s">
                        <h3><?=$v->name?></h3>
                        <!--<h2>ATF PREMIUM</h2>*/-->
                        <div class="premium__custom-cont">
                            <?=$v->content?>
                        </div>
                        <div class="pdf-dowload-section">
                       
                            <? if($v->filea):?>
                            <a href="<?=$v->getFileA();?>" target="_blank"><img src="/public/images/pdf.svg" alt="PDF-image">
                                <p><?=$v->filenamea;?></p> <img src="/public/images/pdf-download.png" alt="PDF-download--image">
                            </a>
							<? endif;?>
                           
                            <? if($v->fileb):?>
                            <a href="<?=$v->getFileB();?>" target="_blank"> <img src="/public/images/pdf.svg" alt="PDF-image">
                                <p><?=$v->filenameb;?></p> <img src="/public/images/pdf-download.png" alt="PDF-download--image">
                            </a>
							<? endif;?>
						</div>
                        <? if($v->preOrder):?>

                            <!-- Button trigger modal -->

                            <button  class="btn--modal-slider feedback" data-toggle="modal" href="#exampleModalLabel-slider">
                                <?=Yii::$app->view->params['translation'][1]->name;?>
                            </button>



                            <!-- Modal -->


                            <!-- Modal -->
                            <div class="submit-modal modal-window-submit">
                                <div class="question-form-item">
                                    <div class="close-modal">
                                        <a href=""><i class="fas fa-times"></i></a>
                                    </div>
                                    <input class="feedbackName_card" type="text" name="name" placeholder="<?=Yii::$app->view->params['translation'][2]->name;?>" required="">
                                    <input class="feedbackPhone_card" type="text" name="phone" placeholder="<?=Yii::$app->view->params['translation'][3]->name;?>" required="">
									
									<input class="feedbackCard_card" type="hidden" name="card" value="<?=$v->name;?>">

                                    <select class="feedbackCity_card">
                                        <? foreach ($contact['city'] as $val):?>
                                            <option value="<?=$val->name?>"><?=$val->name?></option>
                                        <? endforeach;?>
                                    </select>
                                    <div class="button-block">
                                       <!--<a href="#" data-toggle="modal" data-target="#exampleModal-card"><?=Yii::$app->view->params['translation'][4]->name;?></a>-->
                                        <button class="btn-send-request-card" type="submit"><?=Yii::$app->view->params['translation'][5]->name;?></button>
                                    </div>
                                    <div class="button-block--p">
                                        <div class="round">
                                            <input type="checkbox" id="checkbox" />
                                            <label for="checkbox"></label>
                                        </div><?=$contact['agreement']->text;?></div>
                                </div>
                            </div>
                            
                        <?endif;?>
                    </div>
                </div>

                <div class="premium-bottom" id="premium-bottom">

                    <div class="left-side">
                        <ul>
                            <? $size = count($types)/2;?>
                            <? $m=0;?>
                            <? foreach ($types as $value):?>
                                <? $m++;?>
                                <? if($size >= $m):?>
                                    <li><span><img src="<?=$value->getImage();?>"></span><span><?=$value->name;?></span></li>
                                <? endif;?>
                            <? endforeach;?>
                        </ul>
                    </div>


                    <div class="right-side">
                        <ul>
                            <? $size = count($types)/2;?>
                            <? $m=0;?>
                            <? foreach ($types as $value):?>
                                <? $m++;?>
                                <? if($size < $m):?>
                                    <li><span><img src="<?=$value->getImage();?>"></span><span><?=$value->name;?></span></li>
                                <? endif;?>
                            <? endforeach;?>
                        </ul>
                    </div>
                </div>
                <span class="ornament2"><img src="/public/images/ornament.png"></span>
            </div>
		<? endif?>
        <? endforeach;?>
    </div>
    <!--END  PREMIUM-PACKAGE -->


    <!-- PREMIUM-PACKAGE--MOBILE -->

    <div class="glide premium-package2" id="mainCarousel-mob-anchor">

        <div class="glide__track" data-glide-el="track">
            <ul class="glide__slides">
                <? foreach ($card['card'] as $k => $v):?>
				<? if($v->id != 1):?>
                    <? $types = $v->cardItems;?>
                    <li class="glide__slide">
                        <div class="container">
                            <div class="glide__track" data-glide-el="track">


                                <div class="premium-cards">
                                    <img class=" rotateInDownLeft" src="/public/images/cred-cards.png"
                                         data-wow-delay="1.5s">
                                    <div class="promo-video">
                                        <div class="promo-video__parent">
                                            <a class="promo-video" href="<?=$v->url?>">
                                                <?=Yii::$app->view->params['translation'][0]->name;?>
                                            </a>
                                        </div>

                                    </div>
                                </div>

                                <div class="premium-services  fadeInRight" data-wow-delay="1.5s">
                                    <div class="header-premium-mob">
                                        <?=$v->nameMob;?>

                                    </div>
                                    <img src=" <?=$v->getImage();?>">
                                </div>
                                <div class="ul--li">
                                    <?=$v->contentMob;?>
                                </div>
                                <div class="row mob-pdf">
                                    <div class="pdf-dowload-section">
                                   
                                        <? if($v->filea):?>
                                        <a href="<?=$v->getFileA();?>" target="_blank">
                                            <img src="/public/images/pdf.svg">
                                            <p><?=$v->filenamea;?></p> <img src="/public/images/pdf-download.png" alt="PDF-download--image">
                                        </a>
										<? endif;?>
                                        
                                        <? if($v->fileb):?>
                                        <a href="<?=$v->getFileB();?>" target="_blank">
                                            <img src="/public/images/pdf.svg">
                                            <p><?=$v->filenameb;?></p> <img src="/public/images/pdf-download.png" alt="PDF-download--image">
                                        </a>
										<? endif;?>
                                    </div>


                                    <?if($k == 0){?>
                                    <!-- Button trigger modal -->
                                    <div class="collapse-mob">

                                        <a class="btn--modal-slider" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                            <?=Yii::$app->view->params['translation'][1]->name;?>
                                        </a>

                                        <div class="collapse" id="collapseExample">
                                            <div class="card card-body">
                                                <div class="submit-modal-mob">
                                                    <div class="question-form-item">

                                                        <input class="feedbackName_card_mob" type="text" name="name" placeholder="<?=Yii::$app->view->params['translation'][2]->name;?>" required="">
                                                        <input class="feedbackPhone_card_mob" type="text" name="phone" placeholder="<?=Yii::$app->view->params['translation'][3]->name;?>" required="">
														<input class="feedbackCard_card_mob" type="hidden" name="card" value="<?=$v->name;?>">

                                                        <select class="feedbackCity_card_mob">
                                                            <? foreach ($contact['city'] as $val):?>
                                                                <option value="<?=$val->name?>"><?=$val->name?></option>
                                                            <? endforeach;?>
                                                        </select>
                                                        <div class="button-block">
                                                           <!-- <a href="#" data-toggle="modal" data-target="#exampleModal-card-mob"><?=Yii::$app->view->params['translation'][4]->name;?></a>-->
                                                            <button class="btn-send-request-card-mob" type="submit"><?=Yii::$app->view->params['translation'][5]->name;?></button>
                                                        </div>
                                                        <div class="button-block--p">
                                                            <div class="round">
                                                                <input type="checkbox" id="checkbox_mob" />
                                                                <label for="checkbox2"></label>
                                                            </div><?=$contact['agreement']->text;?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?}?>
                                </div>



                              



                                <div id="carouselExampleIndicators<?=$v->id;?>" class="carousel slide custom-carousel__mob"
                                     data-ride="carousel">
                                    <h2 class="inner-sleder--header-title"><?=Yii::$app->view->params['translation'][6]->name;?></h2>
                                    <ol class="carousel-indicators">
                                        <? $active = 'active';?>
                                        <? for($i=0;$i<count($types)/2;$i++):?>
                                            <li data-target="#carouselExampleIndicators<?=$v->id;?>" data-slide-to="<?=$i;?>" class="<?=$active;?>"></li>
                                            <? $active = '';?>
                                        <? endfor;?>
                                    </ol>
                                    <div class="carousel-inner">
                                        <? $active = 'active';?>
                                        <? $m=0;?>
                                        <? foreach ($types as $type):?>
                                            <? $m++;?>
                                            <? if($m % 2 == 1):?>
                                                <div class="carousel-item carousel-item--styles <?=$active?>">
                                            <? endif;?>
                                            <div class="item-box">
                                                <img src="<?=$type->getImage();?>">
                                                <div><?=$type->name;?></div>
                                            </div>
                                            <? if($m % 2 == 0):?>
                                                </div>
                                            <? endif;?>
                                            <? $active = '';?>
                                        <? endforeach;?>
                                        <? if($m % 2 != 0):?>
                                    </div>
                                    <? endif;?>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators<?=$v->id;?>" role="button"
                                   data-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"><i
                                                class="fas fa-chevron-left"></i></span><span class="sr-only">Prev</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators<?=$v->id;?>" role="button"
                                   data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"><i
                                                class="fas fa-chevron-right"></i>
                                    </span><span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>

                    </li>
				<? endif;?>                
				<? endforeach;?>

            </ul>
            <div class="glide__arrows" data-glide-el="controls">
                <a class="custom__glide__arrow  custom__glide__arrow--left" data-glide-dir="<"><img
                            src="/public/images/arrow-prev.png"></a>
                <a class="custom__glide__arrow  custom__glide__arrow--right" data-glide-dir=">"><img
                            src="/public/images/arrow-nxt.png"></a>
            </div>
        </div>
    </div>
    <!-- END PREMIUM-PACKAGE--MOBILE -->

    <!-- PROMOTIONS-AND-DISCOUNTS -->
    <div id="promotions" class="promotions-and-discounts"
         style="background-position: center;background-repeat: no-repeat;-webkit-background-size: cover;background-size: cover;background-image: url(<?=$promotion['sub']->getImage();?>);padding: 15rem 0;">
        <div class="container">

            <div class="promotions-wrapper">
                <div class="promotions-leftside wow fadeInLeft" data-wow-duration="1.5s">
                    <h3><?=$promotion['sub']->name;?></h3>
                    <p><?=$promotion['sub']->content;?></p>
                </div>

                <div class="promotions-rightside wow fadeInRight" data-wow-duration="1.5s">
                    <? foreach ($promotion['main'] as $v):?>
                        <div class="discount-item">
                            <a href="<?=$v->url;?>"><img src="<?=$v->getImageA();?>" style="width: 100%;height: 100%;"></a>
							<p>
								<!--<a href="/site/promotion-view?id=<?=$v->id?>"><?=$v->name;?></a> -->
								<a href="<?=$v->url;?>"><?=$v->name;?></a>
							</p>
                        </div>
                    <? endforeach;?>

                </div>
            </div>
        </div>
    </div>
    <!-- END PROMOTIONS-AND-DISCOUNTS -->

    <!-- NEWS AND ARTICLES -->
    <div class="news-and-articles"
         style="min-height: 700px;background-position: center;background-repeat: no-repeat;-webkit-background-size: cover;background-size: cover;background-image: url(<?=$news['sub']->getImage();?>);padding: 5rem 0;">
        <div class="title">

            <div class="container">
                <h3><?=$news['sub']->name?></h3>
            </div>
        </div>
        <div id="carouselOwl" class="owl-carousel owl-theme news-slider">
            <? foreach ($news['main'] as $v):?>
                <div class="news-card">
                    <img src="<?=$v->getImagea();?>">
                    <div class="news_content">
                        <h4><a href='/site/<?=$v->type == "НОВОСТИ" ? "news":"ads";?>'><?=$v->type;?></a></h4>
                        <h3><a href="/site/news-view?id=<?=$v->id?>"><?=$v->name;?></a></h3>
                        <p><?=$v->subcontent;?></p>
                        <span><?=$v->getDate();?></span>
                    </div>
                </div>
            <? endforeach;?>
        </div>
    </div>
    <!-- END NEWS AND ARTICLES -->


    <!--PROMOTIONS AND NEWS BLOCKS FOR MOBILE-->
    <!-- PROMOTIONS MOBILE -->
    <div class="promotions-mob" id="promotions-mob">
        <div class="container">
            <div class="title">
                <h3><?=$promotion['sub']->name;?></h3>
                <p><?=$promotion['sub']->content;?></p>
            </div>
            <div class="promotion-mobile-wrapper">
                <? foreach ($promotion['main'] as $v):?>
                    <div class="promotion-mobile-item">
                        <div class="promotion-img">
							<a href="<?=$v->url;?>"><img src="<?=$v->getImagea();?>"></a>
                        </div>
                        <p><?=$v->name;?></p>
                        <a class="open-promotion" href="<?=$v->url;?>">
                            <img src="/public/images/open-icon.png">
                        </a>
                    </div>
                <? endforeach;?>
            </div>
            <a class="order-card-btn-mobile" href="#">
                ЗАКАЗАТЬ КАРТУ
            </a>
        </div>
    </div>
    <!-- END PROMOTIONS MOBILE -->

    <!-- NEWS MOBILE -->
    <div class="news-mobile">
        <div class="container">
            <div class="title">
                <h3><h3><?=$news['sub']->name?></h3></h3>
                <p><?=$promotion['sub']->content;?></p>
            </div>
            <div id="mobile-news" class="owl-carousel owl-theme">
                <? foreach ($news['main'] as $v):?>
                    <div class="mobile-news-card">
                        <div class="mobile-news-card__img">
                            <img src="<?=$v->getImagea();?>">
                            <a href="/site/promotion-view?id=<?=$v->id?>"><h3><?=$v->name;?></h3></a>
                        </div>
                        <div class="mobile-news-card__content">
							<p><?=$v->subcontent;?></p>
                            <div class="card-mobile__date">
                                <div class="date-left">
                                    <a href='/site/<?=$v->type == "НОВОСТИ" ? "news":"ads";?>'> <span><?=$v->type;?></span></a>
                                </div>
                                <div class="date-right">
                                    <span><?=$v->getDate();?></span>
                                </div>
                            </div>
                        </div>
                    </div>

                <? endforeach;?>

            </div>
            <ul class="carousel-navigation">
                <li id="news-slider-prev"><img src="/public/images/nav-btn.png"></li>
                <li id="news-slider-next"><img src="/public/images/nav-btn.png"></li>
            </ul>
        </div>
    </div>
    <!-- END NEWS MOBILE -->
    <!--END PROMOTIONS AND NEWS BLOCKS FOR MOBILE-->


    <!-- BLOCK QUESTIONS -->
    <div id="apply" class="block-questions"
         style="    background-image: url(<?=$request['sub']->getImage()?>);-webkit-background-size: cover;background-size: cover;background-position: center;background-repeat: no-repeat;padding: 70px 0;">
        <span class="ornament3"><img src="/public/images/ornament2.png"></span>
        <div class="container">
            <div class="title wow fadeInLeft" data-wow-duration="1.5s">
                <h3><?=$request['sub']->name?></h3>
                <p><?=$request['sub']->content?></p>
            </div>

            <div class="question-form wow fadeInRight" data-wow-duration="1.5s">

                <div class="question-form-item">
                    <input class="feedbackName" type="text" name="name" placeholder="<?=Yii::$app->view->params['translation'][2]->name;?>" required>
                    <input class="feedbackPhone" type="text" name="phone" placeholder="<?=Yii::$app->view->params['translation'][3]->name;?>" required>
                    <select class="feedbackCity">
                        <? foreach ($contact['city'] as $v):?>
                            <option value="<?=$v->name?>"><?=$v->name?></option>
                        <? endforeach;?>
                    </select>
                    <div class="button-block">
                        <a href="#" data-toggle="modal" data-target="#exampleModal"><?=Yii::$app->view->params['translation'][4]->name;?></a>
                        <button class="btn-send-request" type="submit"><?=Yii::$app->view->params['translation'][5]->name;?></button>
                    </div>
                </div>
            </div>
            <script>
                $(".feedbackName").keyup(function (e) {
                    var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
                    if (regex.test(this.value) !== true)
                        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
                });
                $('.feedbackPhone').inputmask("(999) 999-9999");
				
				$(".feedbackName_card").keyup(function (e) {
					var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
					if (regex.test(this.value) !== true)
						this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
				});
				$('.feedbackPhone_card').inputmask("(999) 999-9999");

				$(".feedbackName_card_mob").keyup(function (e) {
					var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
					if (regex.test(this.value) !== true)
						this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
				});
				$('.feedbackPhone_card_mob').inputmask("(999) 999-9999");
            </script>
        </div>
    </div>
    <!-- Button trigger modal -->

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <? if(date('H:i:s') > $workTime->to || date('H') < $workTime->from):?>
                        <?=Yii::$app->view->params['translation'][7]->name;?>
                    <? endif;?>
                    <div class="selec-date">
                        <div class="select">
                            <select class="feedbackHour" name="hour" id="slct">
                                <option value=""></option>
                                <?for($i=$workTime->from;$i<$workTime->to;$i++):?>
                                    <option value="<?=$i?>"><?=$i?></option>
                                <? endfor;?>
                            </select>
                        </div>
                        <div class="select">
                            <select class="feedbackMinute" name="minute" id="slct2">
                                <option value=""></option>
                                <option value="00">00</option>
                                <option value="15">15</option>
                                <option value="30">30</option>
                                <option value="45">45</option>
                            </select>
                        </div>
                        <div class="select">
                            <select class="feedbackDay" name="day" id="slct3">
                                <option value=""></option>
                                <option value="today"><?=Yii::$app->view->params['translation'][8]->name;?></option>
                                <option value="tomorrow"><?=Yii::$app->view->params['translation'][9]->name;?></option>
                            </select>
                        </div>
                    </div>

                    <div class="tel-number">
                        <button class="btn-send-request" id="btn-send-request" type="submit" name="button"
                                data-dismiss="modal" aria-label="Close"><?=Yii::$app->view->params['translation'][10]->name;?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="modal fade" id="exampleModal-card" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <? if(date('H:i:s') > $workTime->to || date('H') < $workTime->from):?>
                        <?=Yii::$app->view->params['translation'][7]->name;?>
                    <? endif;?>
                    <div class="selec-date">
                        <div class="select">
                            <select class="feedbackHour_card" name="hour" id="slct">
                                <option value=""></option>
                                <?for($i=$workTime->from;$i<$workTime->to;$i++):?>
                                    <option value="<?=$i?>"><?=$i?></option>
                                <? endfor;?>
                            </select>
                        </div>
                        <div class="select">
                            <select class="feedbackMinute_card" name="minute" id="slct2">
                                <option value=""></option>
                                <option value="00">00</option>
                                <option value="15">15</option>
                                <option value="30">30</option>
                                <option value="45">45</option>
                            </select>
                        </div>
                        <div class="select">
                            <select class="feedbackDay_card" name="day" id="slct3">
                                <option value=""></option>
                                <option value="today"><?=Yii::$app->view->params['translation'][8]->name;?></option>
                                <option value="tomorrow"><?=Yii::$app->view->params['translation'][9]->name;?></option>
                            </select>
                        </div>
                    </div>

                    <div class="tel-number">
                        <button class="btn-send-request-card" id="btn-send-request" type="submit" name="button"
                                data-dismiss="modal" aria-label="Close"><?=Yii::$app->view->params['translation'][10]->name;?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="modal fade" id="exampleModal-card-mob" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <? if(date('H:i:s') > $workTime->to || date('H') < $workTime->from):?>
                        <?=Yii::$app->view->params['translation'][7]->name;?>
                    <? endif;?>
                    <div class="selec-date">
                        <div class="select">
                            <select class="feedbackHour_mob" name="hour" id="slct">
                                <option value=""></option>
                                <?for($i=$workTime->from;$i<$workTime->to;$i++):?>
                                    <option value="<?=$i?>"><?=$i?></option>
                                <? endfor;?>
                            </select>
                        </div>
                        <div class="select">
                            <select class="feedbackMinute_mob" name="minute" id="slct2">
                                <option value=""></option>
                                <option value="00">00</option>
                                <option value="15">15</option>
                                <option value="30">30</option>
                                <option value="45">45</option>
                            </select>
                        </div>
                        <div class="select">
                            <select class="feedbackDay_mob" name="day" id="slct3">
                                <option value=""></option>
                                <option value="today"><?=Yii::$app->view->params['translation'][8]->name;?></option>
                                <option value="tomorrow"><?=Yii::$app->view->params['translation'][9]->name;?></option>
                            </select>
                        </div>
                    </div>

                    <div class="tel-number">
                        <button class="btn-send-request-card-mob" id="btn-send-request" type="submit" name="button"
                                data-dismiss="modal" aria-label="Close"><?=Yii::$app->view->params['translation'][10]->name;?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END BLOCK QUESTIONS -->

    <script>
        $('body').on('click', ".btn-send-request", function () {
            var name = $(".feedbackName").val();
            var phone = $(".feedbackPhone").val();
            var city = $(".feedbackCity").val();
            var hour = $(".feedbackHour").val();
            var minute = $(".feedbackMinute").val();
            var day = $(".feedbackDay").val();

            if (!name) {
                swal('<?=Yii::$app->view->params['translation'][16]->name;?>!', '<?=Yii::$app->view->params['translation'][11]->name;?>', 'error');
            }else if (!phone) {
                swal('<?=Yii::$app->view->params['translation'][16]->name;?>!', '<?=Yii::$app->view->params['translation'][12]->name;?>', 'error');
            } else if (!city) {
                swal('<?=Yii::$app->view->params['translation'][16]->name;?>!', '<?=Yii::$app->view->params['translation'][13]->name;?>', 'error');
            } else {
				 swal('<?=Yii::$app->view->params['translation'][14]->name;?>', '<?=Yii::$app->view->params['translation'][15]->name;?>', 'success');
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: "/site/request",
                    data: { name: name, phone: phone, city: city, hour: hour, minute: minute, day: day },
                    success: function (data) {
                        if (data.status) {
                           
                        }else {
                            swal('Упс!', 'Что-то пошло не так.', 'error');
                        }
                    },
                    error: function () {
                        swal('Упс!', 'Что-то пошло не так.', 'error');
                    }
                });

                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: "/site/request-email",
                    data: { name: name, phone: phone, city: city, hour: hour, minute: minute, day: day },
                    success: function (data) {

                    },
                    error: function () {
                        swal('Упс!', 'Что-то пошло не так.', 'error');
                    }
                });
            }
        });
    </script>

   
	
    <!-- ADDRESS AND CONTACTS START -->
    <div id="address" class="address">
        <div class="row">

            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="row">
                    <div class="mobile col-sm-12 col-md-12 col-lg-7">
                        <div class="address__select-city--mob">
                            <h2><?=Yii::$app->view->params['translation'][17]->name;?></h2>
                            <select class="city-select" href="#">
                                <? foreach ($contact['select'] as $k => $v):?>
                                    <option value="<?=$v->city_id?>" <?=$v->city_id == 1 ? "selected":"";?>><a href="#"> <?=$v->cityName;?></a></option>
                                <? endforeach;?>
                            </select>
                            <i class="fas fa-sort-down"></i>
                        </div>
                        <div id="map" style="width: 100%; height: 100%"></div>
                        <script>
                            //document.getElementById("office-city").innerHTML="<?//=$city->name?>//";
                            ymaps.ready(init);
                            var map = "";
                            function init() {
                                map = new ymaps.Map('map', {
                                    center: [43.238293, 76.945465],
                                    zoom: 11,
                                });

                                var myGeocoder = ymaps.geocode("Алматы");
                                myGeocoder.then(
                                    function (res) {
                                        var street = res.geoObjects.get(0);
                                        var coords = street.geometry.getCoordinates();
                                        map.setCenter(coords);
                                    },
                                    function (err) {

                                    }
                                );

                                <? foreach($contact['data'] as $data) :?>
                                    <? foreach($data as $v) :?>
                                        <? if($v->city_id == 1):?>
                                        map.geoObjects.add(new ymaps.Placemark([<?= $v -> latitude ?>, <?= $v -> longitude ?>], {
                                            balloonContent: '<div class="company-name wow fadeInUp"><?=$v->address?></br><?=$v->workTime?></br><?=$v->telephone?></div>'
                                        }, {
                                            iconLayout: 'default#image',
                                            iconImageHref: '/public/images/loc.png',
                                            iconImageSize: [22, 30],
                                            preset: 'islands#icon',
                                            iconColor: '#0095b6'
                                        }));
                                        <? endif;?>
                                    <? endforeach;?>
                                <? endforeach;?>

                                $('.city-select').change(function (e) {

                                    test = $(this).val();
                                    e.preventDefault();
                                    map.geoObjects.removeAll();

                                    $.ajax({
                                        type: "GET",
                                        url: "/site/get-city-name",
                                        data: { id: test },
                                        success: function (rez) {

                                            var myGeocoder = ymaps.geocode(rez);
                                            myGeocoder.then(
                                                function (res) {
                                                    // map.geoObjects.add(res.geoObjects);
                                                    var street = res.geoObjects.get(0);
                                                    var coords = street.geometry.getCoordinates();
                                                    map.setCenter(coords);
                                                },
                                                function (err) {

                                                }
                                            );
                                        }
                                    });

                                    $.ajax({
                                        type: "GET",
                                        dataType: "json",
                                        url: "/site/city",
                                        data: { id: test},
                                        success: function (data) {
                                            for (i = 0; i < data.length; i++) {
                                                latitude = data[i][0];
                                                longitude = data[i][1];
                                                address = data[i][2];
                                                workTime = data[i][3];
                                                telephone = data[i][4];

                                                map.geoObjects.add(new ymaps.Placemark([latitude, longitude], {
                                                    balloonContent: '<div class="company-name  fadeInUp">' + address + '</br>' + workTime + '</br>' + telephone + '</div>'
                                                }, {
                                                    iconLayout: 'default#image',
                                                    iconImageHref: '/public/images/loc.png',
                                                    iconImageSize: [22, 30],
                                                    preset: 'islands#icon',
                                                    iconColor: '#0095b6'
                                                }));
                                            }
                                        },
                                        error: function () {
                                            swal('Упс!', 'Что-то пошло не так.', 'error');
                                        }
                                    });


                                    $.ajax({
                                        url: "/site/get-location-content-count",
                                        data: {id:test},
                                        type: 'GET',
                                        success: function (response) {
                                            if(response < 4) {
                                                $('#address_content').removeClass('address__locations');
                                            }
                                            else {
                                                $('#address_content').addClass("address__locations");
                                            }
                                        },
                                        error: function () {
                                            swal('Упс!', 'Что-то пошло не так.', 'error');
                                        }
                                    });


                                    $.ajax({
                                        url: "/site/get-location-content",
                                        data: {id:test},
                                        type: 'GET',
                                        success: function (response) {
                                            $('#address_content').html(response);

                                        },
                                        error: function () {
                                            swal('Упс!', 'Что-то пошло не так.', 'error');
                                        }
                                    });




                                    $.ajax({
                                        url: "/site/get-location-content-mob",
                                        data: {id:test},
                                        type: 'GET',
                                        success: function (response) {
                                            $('#address_content_mob').html(response);
                                        },
                                        error: function () {
                                            swal('Упс!', 'Что-то пошло не так.', 'error');
                                        }
                                    });

                                });

                            }
                        </script>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-5 address__mobile-contacts" id="address__right"
                         style="padding: 3rem;background-image: url(<?=$contact['sub']->getImage();?>);background-repeat: no-repeat;-webkit-background-size: cover;background-size: cover;">
                        <span class="ornament4"><img src="/public/images/ornament3.png"></span>
                        <div class="address__select-city">
                            <select class="city-select" href="#">
                                <? foreach ($contact['select'] as $k => $v):?>
                                    <option value="<?=$v->city_id?>" <?=$v->city_id == 1 ? "selected":"";?>><a href="#"> <?=$v->cityName;?></a></option>
                                <? endforeach;?>
                            </select>
                            <img class="slt-down" src="/public/images/arrow_down.png">
                        </div>

                        <? foreach ($contact['data'] as $k => $data):?>
                            <? if($k == 1):?>
                                <div class="<?=count($data) > 4 ? "address__locations":"";?>" id="address_content">
                                <? $m=0;?>
                                <? foreach($data as $v):?>
                                        <? $m++;?>
                                        <div class="adress__locations__items">
                                            <div class="row">
                                                <div class="col-sm-5 col-md-4 col-lg-1">
                                                    <div class="address__locations__items__circle--white"><?=$m?></div>
                                                </div>
                                                <div class="col-sm-7 col-md-8 col-lg-11">
                                                    <p><?=$v->address?><br>
                                                        <?=$v->workTime?><br>
                                                        <?=$v->telephone?></p>
                                                </div>
                                            </div>
                                        </div>

                                <? endforeach;?>
                                </div>
                            <? break;?>
                            <? endif;?>
                        <? endforeach;?>

                        <? foreach ($contact['data'] as $k => $data):?>
                            <? if($k == 1):?>
                            <div class="address__locations-mob" id="address_content_mob" >
                                <? $m=0;?>
                                <? foreach ($data as $v):?>
                                    <? $m++;?>
                                    <div class="adress__locations__items">
                                        <div class="row">
                                            <div class="col-sm-5 col-md-4 col-lg-1">
                                                <div class="address__locations__items__circle--white">
                                                    <h4> <?=$m;?></h4>
                                                </div>
                                                <div class="address__locations__items__circle__mobile--white">
                                                    <i class="fas fa-map-marker-alt"></i>
                                                    <h4> <?=$v->address?></h4>
                                                </div>
                                            </div>
                                            <div class="col-sm-7 col-md-8 col-lg-11">
                                                <div class="mob-version__address">

                                                    <p>   <?=$v->workTime?>
                                                        <?=$v->telephone?> </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach;?>
                                </div>
                            <? break;?>
                            <? endif;?>
                        <? endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ADDRESS AND CONTACTS END -->
</main>
