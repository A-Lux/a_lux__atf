<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 15.04.2019
 * Time: 10:43
 */

?>

<!-- MAIN -->
<main class="main">
    <!-- ARCHIVE NEWS -->
    <div id="archive-news" class="archive-news">
        <div class="container">
            <!-- хлебные крошки -->
            <div class="bread-crumbs">
                <ul>
                    <li><a href="<?=Yii::$app->view->params['main']->url?>"><?=Yii::$app->view->params['main']->text;?></a></li>
                    <li><a><?=$model->text;?></a></li>
                </ul>
            </div>
            <!-- end хлебные крошки -->
            <div class="title">
                <h3><?=$model->text;?></h3>
            </div>

            <div class="arch-news-wrapper">

                <? foreach ($news['data'] as $v):?>
                    <div class="news-card">
                        <img src="<?=$v->getImagea();?>">
                        <div class="news_content">
                            <h4><?=$v->type;?></h4>
                            <h3><a href="/site/news-view?id=<?=$v->id?>"><?=$v->name;?></a></h3>
                            <p><?=$v->subcontent;?></p>
                            <span><?=$v->getDate();?></span>
                        </div>
                    </div>
                <? endforeach;?>
            </div>

            <?= $this->render('/partials/pagination', [
                'pagination'=>$news['pagination'],
            ]);?>

        </div>
    </div>
    <!-- END ARCHIVE NEWS -->
</main>
<!-- END MAIN -->
