<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 18.03.2019
 * Time: 17:58
 */


use yii\helpers\Url;
Yii::$app->view->params['title'] = Yii::$app->view->params['menu'][2]->metaName;
Yii::$app->view->params['desc'] = Yii::$app->view->params['menu'][2]->metaDesc;
Yii::$app->view->params['key'] = Yii::$app->view->params['menu'][2]->metaKey;
?>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<main>
    <div class="bg-white">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="bread-crumbs wow fadeInLeft">
                        <ul>
                            <li><a href="<?=Url::toRoute([Yii::$app->view->params['menu'][0]->url])?>">Главная</a></li>
                            <li>/</li>
                            <li><?=Yii::$app->view->params['menu'][2]->text?></li>
                        </ul>
                    </div>
                    <div class="inner-title wow flipInX">
                        <h3>ДИЛЕРЫ</h3>
                    </div>
                </div>
                <div class="col-sm-9">
                    <form class="inner-search wow fadeInUp" action="/site/dealer-search" method="get">
                        <input type="text" name="text" placeholder="Почтовый индекс, город или область" required>
                        <button><img src="/public/images/inner-search.png" alt="">Найти</button>
                    </form>

                    <? if($type == 'search'):?>
                        <? if(count($dealers) == 0):?>
                            <div class="" style="font-size: 22px;margin: 20px 0 20px 0;">По запросу <span style="font-weight: bold;"><?=$text?></span> ничего не найдено.</div>
                        <? endif;?>

                        <? if(count($dealers) != 0):?>
                            <div class="" style="font-size: 22px;margin: 20px 0 20px 0;">Результаты поиска по запросу <span style="font-weight: bold;"><?=$text?></span>.</div>
                        <? endif;?>

                    <? endif;?>
                    <div class="map wow fadeInUp">
                        <div id="map" style="width: 100%; height: 300px"></div>
                        <script>
                            //document.getElementById("office-city").innerHTML="<?//=$city->name?>//";
                            ymaps.ready(init);
                            var center_map = [0, 0];
                            var map = "";
                            function init() {
                                map = new ymaps.Map('map', {
                                    center: center_map,
                                    zoom: 4,
                                });

                                var myGeocoder = ymaps.geocode("Казахстан");
                                myGeocoder.then(
                                    function (res) {
                                        var street = res.geoObjects.get(0);
                                        var coords = street.geometry.getCoordinates();
                                        map.setCenter(coords);
                                    },
                                    function (err) {

                                    }
                                );

                                <?foreach($dealers as $v){?>
                                map.geoObjects.add(new ymaps.Placemark([<?=$v->latitude?>, <?=$v->longitude?>], {
                                    balloonContent: '<div class="company-name wow fadeInUp"><h6><?=$v->name?></h6><?=$v->address?></br><?=$v->telephone?></div>'
                                }, {
                                    iconLayout: 'default#image',
                                    iconImageHref: '/public/images/Sika-logo-2018.png',
                                    preset: 'islands#icon',
                                    iconColor: '#0095b6'
                                }));
                                <?}?>
                            }
                        </script>
                    </div>
                    <div class="row">
                        <div class="col-sm-6" >
                            <?$m=0;?>
                            <?foreach ($dealers as $v):?>
                                <?if(count($dealers)/2 > $m):?>
                                <div class="company-name wow fadeInUp">
                                    <h6><?=$v->name?></h6>
                                    <p><?=$v->address?><br></p>
                                    <p><a href="tel:<?=$v->telephone?>"><?=$v->telephone?></a></p>
                                    <?=$v->content?>
                                </div>
                                <?endif;?>
                                <?$m++?>
                            <? endforeach;?>
                        </div>
                        <div class="col-sm-6">
                            <?$m=0;?>
                            <?foreach ($dealers as $v):?>
                                <?if(count($dealers)/2 <= $m):?>
                                    <div class="company-name wow fadeInUp">
                                        <h6><?=$v->name?></h6>
                                        <p><?=$v->address?><br></p>
                                        <p><a href="tel:<?=$v->telephone?>"><?=$v->telephone?></a></p>
                                        <?=$v->content?>
                                    </div>
                                <?endif;?>
                                <?$m++?>
                            <? endforeach;?>
                        </div>
                    </div>
                </div>
                <?=$this->render('/partials/banner');?>
            </div>
        </div>
    </div>

</main>
