<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "worktime".
 *
 * @property int $id
 * @property int $from
 * @property int $to
 */
class Worktime extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'worktime';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['from', 'to'], 'required'],
            [['from', 'to'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from' => 'От',
            'to' => 'До',
        ];
    }
}
