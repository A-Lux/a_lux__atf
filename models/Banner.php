<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "banner".
 *
 * @property int $id
 * @property string $name
 * @property string $subname
 * @property string $content
 * @property string $image
 * @property string $buttonName
 * @property string $url
 */
class Banner extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banner'.Yii::$app->session["lang"];;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'subname', 'content', 'buttonName', 'url'], 'required'],
            [['content'], 'string'],
            [['name', 'subname', 'buttonName', 'url'], 'string', 'max' => 255],
            [['image'],'file','extensions'=>'png,jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Заговок',
            'subname' => 'Подзаголовок',
            'content' => 'Контент',
            'image' => 'Картинка',
            'buttonName' => 'Название кнопка',
            'url' => 'Ссылка',
        ];
    }

    public function saveImage($filename)
    {
        $this->image = $filename;
        return $this->save(false);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/' . $this->image : '/no-image.png';
    }

}
