<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logo".
 *
 * @property int $id
 * @property string $image
 * @property string $copyright
 */
class Logo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logo'.Yii::$app->session["lang"];;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['copyright'], 'required'],
            [['copyright'], 'string', 'max' => 255],
            [['image'],'file','extensions'=>'png,jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Картинка',
            'copyright' => 'Копирайт',
        ];
    }

    public function saveImage($filename)
    {
        $this->image = $filename;
        return $this->save(false);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/' . $this->image : '/no-image.png';
    }

}
