<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "contact".
 *
 * @property int $id
 * @property string $address
 * @property string $workTime
 * @property string $telephone
 * @property string $latitude
 * @property string $longitude
 * @property int $city_id
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact'.Yii::$app->session["lang"];;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address', 'workTime', 'telephone', 'latitude', 'longitude', 'city_id'], 'required'],
            [['city_id'], 'integer'],
            [['address', 'workTime', 'telephone', 'latitude', 'longitude'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address' => 'Адрес',
            'workTime' => 'Время работа',
            'telephone' => 'Телефон',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'city_id' => 'Город',
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    public function getCityName(){
        return (isset($this->city))? $this->city->name:'Не задан';
    }

    public static function getContactWithCity(){
        $model = Contact::find()->all();
        $arr = array();
        foreach ($model as $k=>$v){
            if(ArrayHelper::keyExists($v->city_id,$arr)){
                array_push($arr[$v->city_id],$v);
            }else{
                $ar = array();
                array_push($ar,$v);
                $arr[$v->city_id] = $ar;
            }

        }

        return $arr;

    }
}
