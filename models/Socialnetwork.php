<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "socialnetwork".
 *
 * @property int $id
 * @property string $facebook
 * @property string $instagram
 * @property string $twitter
 */
class Socialnetwork extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'socialnetwork';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['facebook', 'instagram', 'twitter'], 'required'],
            [['facebook', 'instagram', 'twitter'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'facebook' => 'Facebook',
            'instagram' => 'Instagram',
            'twitter' => 'Twitter',
        ];
    }
}
