<?php

namespace app\models;

use DateTime;
use Yii;

/**
 * This is the model class for table "request".
 *
 * @property int $ID
 * @property string $UF_FIO
 * @property string $UF_CITY
 * @property string $UF_PHONE
 * @property string $date
 * @property string $time
 */
class Request extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'request';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
//            [['UF_FIO', 'UF_CITY', 'UF_PHONE', 'date', 'time'], 'required'],
            [['date', 'time','UF_DATE'], 'safe'],
            [['UF_FIO', 'UF_CITY', 'UF_PHONE','UF_PRODUCT','UF_IP'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'UF_FIO' => 'ФИО',
            'UF_CITY' => 'Город',
            'UF_PHONE' => 'Телефон',
            'date' => 'Дата',
            'UF_DATE' => 'Date Time',
            'time' => 'Время',
            'UF_PRODUCT' => 'Продукт',
            'UF_IP' => 'IP-адрес'
        ];
    }

    public function saveRequest($name,$phone,$city, $hour, $minute, $day, $card = "«Premium»"){

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $this->UF_FIO = $name;
        $this->UF_PHONE = $phone;
        $this->UF_CITY = $city;
        Yii::$app->formatter->locale = 'ru-RU';
        $this->UF_DATE = date('Y-m-d H:i:s');
        $this->UF_PRODUCT = $card;
        $this->UF_IP = $ip;

        if($day == 'today'){
            $this->date = date('Y-m-d');
        }else if($day == 'tomorrow'){
            try {
                $datetime = new DateTime('tomorrow');
                $this->date = $datetime->format('Y-m-d');
            } catch (\Exception $e) {
            }
        }else{
            $this->date = '';
        }

        if($hour != null && $minute != null){
            $this->time =  date('H:i', strtotime("$hour:$minute"));
        }else{
            $this->time = '';
        }

        return $this->save(false);
    }
}
