if (window.location.pathname === "/site/promotion-view" || window.location.pathname === "/site/news-view" ) {
    window.scrollTo({
        top: 586,
        behavior: "smooth"
    });
}

if (window.location.pathname != "/") {
    document.querySelector(".btn-more").addEventListener("click", function () {
        location.replace("/site/index#apply") 
    })
}

$(document).ready(function () {
	 $('a[href*=\\#]').on('click', function(e){
        e.preventDefault();
        $('html, body').animate({
            scrollTop : $(this.hash).offset().top
        }, 500);
    });
	
	$(".submit-modal").hide();
	$(".feedback").click(function(e) {

		e.preventDefault();
		$('.submit-modal').show();
	})

	$(".close-modal").click( function(e) {
		e.preventDefault();
		$(".submit-modal").hide();
	})

	$(".submit-modal-video").hide();
	$(".feedback-video").click(function(e) {
		e.preventDefault();
		$('.submit-modal-video').show();
	})

	$(".close-modal-video").click( function(e) {
		e.preventDefault();
		$(".submit-modal-video").hide();
	})
    var newsOwl = $(".news-slider");
	var mainCarousel = $('#mainCarousel');
	var mobNews = $('#mobile-news');
    newsOwl.owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        nav: true,
		smartSpeed: 1000,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            },
        }
    });
	mainCarousel.owlCarousel({
      loop: true,
      margin: 10,
      responsiveClass: true,
      nav: true,
      responsive: {
          0: {
              items: 1
          },
          600: {
              items: 1
          },
          1000: {
              items: 1
          },
      }
    });
    mobNews.owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        nav: false,
        smartSpeed: 800,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            },
        }
    });
    $('#news-slider-prev').click(function(e) {
      e.preventDefault();
      mobNews.trigger('prev.owl.carousel', [300]);
    })

    $('#news-slider-next').click(function(e) {
      e.preventDefault();
      mobNews.trigger('next.owl.carousel');
    })
});

$(document).ready(function () {
    var newsOwl = $(".news-slider");
	var mainCarousel = $('#mainCarousel-mob');
    newsOwl.owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        nav: true,
		smartSpeed: 1000,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 2,
				
            },
            1000: {
                items: 3
            },
        }
    });
	mainCarousel.owlCarousel({
      loop: true,
      margin: 10,
      responsiveClass: true,
      nav: true,
      responsive: {
          0: {
              items: 1
          },
          600: {
              items: 1
          },
          1000: {
              items: 1
          },
      }
    });
});

$('.carousel').carousel({

	responsive: {
            0: {
                interval: false
            },
            600: {
                interval: 3000
            },
            1000: {
                interval: 3000
            },
	}
});

/*$(document).ready(function() {
    // GALLERY VIDEO MAGNIFIC
    $(function() {
        $('.promo-video').magnificPopup({
            disableOn: 700,
            delegate: 'a',
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            zoom: true,
            fixedContentPos: false
        });
    });
    // END GALLERY VIDEO MAGNIFIC
});*/
(function ($) {

    new WOW().init();

    $('.bar-btn').click(function () {
        $('.bar-list').show();
    });

   /* $(document).ready(function () {
        $('.date').mask('00/00/0000');
        $('.time').mask('00:00:00');
        $('.date_time').mask('00/00/0000 00:00:00');
        $('.cep').mask('00000-000');
        $('.phone').mask('0000-0000');
        $('.index').mask('000000');
        $('.phone_with_ddd').mask('(00) 0000-0000');
        $('.phone_us').mask('+7(000) 000-0000');
        $('.mixed').mask('AAA 000-S0S');
        $('.cpf').mask('000.000.000-00', { reverse: true });
        $('.cnpj').mask('00.000.000/0000-00', { reverse: true });
        $('.money').mask('000.000.000.000.000,00', { reverse: true });
        $('.money2').mask("#.##0,00", { reverse: true });
        $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
            translation: {
                'Z': {
                    pattern: /[0-9]/, optional: true
                }
            }
        });
        $('.ip_address').mask('099.099.099.099');
        $('.percent').mask('##0,00%', { reverse: true });
        $('.clear-if-not-match').mask("00/00/0000", { clearIfNotMatch: true });
        $('.placeholder').mask("00/00/0000", { placeholder: "__/__/____" });
        $('.fallback').mask("00r00r0000", {
            translation: {
                'r': {
                    pattern: /[\/]/,
                    fallback: '/'
                },
                placeholder: "__/__/____"
            }
        });
        $('.selectonfocus').mask("00/00/0000", { selectOnFocus: true });
    });*/

    var clOffCanvas = function () {

        var menuTrigger = $('.header-menu-toggle'),
            nav = $('.header-nav'),
            closeButton = nav.find('.header-nav__close'),
            siteBody = $('body'),
            mainContents = $('section, footer');

        // open-close menu by clicking on the menu icon
        menuTrigger.on('click', function (e) {
            e.preventDefault();
            // menuTrigger.toggleClass('is-clicked');
            siteBody.toggleClass('menu-is-open');
        });

        // close menu by clicking the close button
        closeButton.on('click', function (e) {
            e.preventDefault();
            menuTrigger.trigger('click');
        });

        // close menu clicking outside the menu itself
        siteBody.on('click', function (e) {
            if (!$(e.target).is('.header-nav, .header-nav__content, .header-menu-toggle, .header-menu-toggle span')) {
                // menuTrigger.removeClass('is-clicked');
                siteBody.removeClass('menu-is-open');
            }
        });

    };

    /* Initialize
     * ------------------------------------------------------ */
    (function ssInit() {
        clOffCanvas();
    })();


})(jQuery);
function add() {
    let x;
    x = document.getElementById("width").value;
    x++;
    document.getElementById("width").innerHTML = x;
    // END

}

// ONLINE PAY
$(function () {
    $('.online-pay ul').hide();
    $('.online-pay-btn').on('click', function (e) {
        e.preventDefault()
        $('.online-pay ul').slideToggle(500);
        if (!$(this).hasClass('display')) {
            $(this).addClass('display');
            $('.online-pay').css('top', '200px');
            $('.online-pay').css('width', '250px');
        }
        else {
            $('.online-pay').css('top', '350px');
            $('.online-pay').css('width', '');
            $(this).removeClass('display');
        }
    })
})
// END ONLINE PAY

// TABS
$(document).ready(function () {

    $('ul.tabs li').click(function () {
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#" + tab_id).addClass('current');
    })

})
// END TABS

$(document).ready(function () {
    $('.minus').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.plus').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });
});


$( '.header-navigation ul a'  ).on( 'click', function(e){

    // Define variable of the clicked »a« element (»this«) and get its href value.
    var href = $(this).attr( 'href' );

    // Run a scroll animation to the position of the element which has the same id like the href value.
    $( 'html, body' ).animate({
        scrollTop: $( href ).offset().top
    }, '500' );

    // Prevent the browser from showing the attribute name of the clicked link in the address bar
    e.preventDefault();

});

$( '.header-nav__content ul a'  ).on( 'click', function(e){

    // Define variable of the clicked »a« element (»this«) and get its href value.
    var href = $(this).attr( 'href' );

    // Run a scroll animation to the position of the element which has the same id like the href value.
    $( 'html, body' ).animate({
        scrollTop: $( href ).offset().top
    }, '500' );

    // Prevent the browser from showing the attribute name of the clicked link in the address bar
    e.preventDefault();

});


$('.owl-carousel--inner').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
    }
})




$(document).ready(function(){
  $('.mob-inner').slick({
 dots: true,
  infinite: true,
  });
});

$(document).on('ready', function() {
    $(".slider-main").slick({
        arrows: true,
        dots: false,
        adaptiveHeight: true,
        slidesToShow: 1
    });
    $(".slider-submain").slick({
        arrows: true,
        dots: true,
        adaptiveHeight: true ,
        slidesToShow: 1,
        draggable: false
    });
});

new Glide('.glide').mount();
new Glide('.glide', {
  type: 'carousel',
  startAt: 0,
  perView: 1
});
new Glide('.glide2').mount();

//Modal carousel
/*
var modal = document.querySelector(".modal");
    var trigger = document.querySelector(".trigger");
    var closeButton = document.querySelector(".close-button");

    function toggleModal() {
        modal.classList.toggle("show-modal");
    }

    function windowOnClick(event) {
        if (event.target === modal) {
            toggleModal();
        }
    }

    trigger.addEventListener("click", toggleModal);
    closeButton.addEventListener("click", toggleModal);
    window.addEventListener("click", windowOnClick);
*/
