<?php

use app\models\City;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contact */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contact-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'workTime')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'latitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'longitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city_id')->dropDownList(\yii\helpers\ArrayHelper::map(City::find()->all(), 'id', 'name'), ['prompt' => '']) ?>


    <div id="map" style="width: 100%; height: 250px"></div>

    <div class="form-group" style="padding-bottom: 20px;padding-top: 20px;">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>


    <script>
        ymaps.ready(init);
        var center_map = [0, 0];
        var map = "";
        function init() {
            map = new ymaps.Map('map', {
                center: center_map,
                zoom: 8,
            });

            <?php if(empty($model->longitude)&&empty($model->latitude)){ ?>
            var myGeocoder = ymaps.geocode("<?=$model->city->name?>");
            myGeocoder.then(
                function (res) {
//                    map.geoObjects.add(res.geoObjects);
                    var street = res.geoObjects.get(0);
                    var coords = street.geometry.getCoordinates();
                    map.setCenter(coords);
                },
                function (err) {

                }
            );
            <?php }else{ ?>
            map.setCenter([<?=$model->latitude?>, <?=$model->longitude?>]);

            map.geoObjects.add(new ymaps.Placemark([<?=$model->latitude?>, <?=$model->longitude?>], {
                balloonContent: ''
            }, {
                preset: 'islands#icon',
                iconColor: '#0095b6'
            }));
            <?php } ?>
            $('#contact-city_id').change(function(){
                $.ajax({
                    type: "GET",
                    url: "/admin/contact/city",
                    data: {id: $(this).val()},
                    success: function(rez){
                        map.geoObjects.removeAll();
                        var myGeocoder = ymaps.geocode(rez);
                        myGeocoder.then(
                            function (res) {
//                                map.geoObjects.add(res.geoObjects);
                                var street = res.geoObjects.get(0);
                                var coords = street.geometry.getCoordinates();
                                map.setCenter(coords);
                            },
                            function (err) {

                            }
                        );
                    }
                });
            });
            map.events.add('click', function (e) {
                map.geoObjects.removeAll();
                var coords = e.get('coords');
                map.geoObjects.add(new ymaps.Placemark(coords, {
                    balloonContent: ''
                }, {
                    preset: 'islands#icon',
                    iconColor: '#0095b6'
                }));
//                            map.setCenter(coords);
                $("#contact-latitude").val(coords[0].toPrecision(9));
                $("#contact-longitude").val(coords[1].toPrecision(9));
            });
        }
    </script>

    <?php ActiveForm::end(); ?>

</div>
