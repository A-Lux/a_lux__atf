<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CreditcardtypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' Содержание';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="creditcardtype-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
//            'image',

            ['attribute'=>'creditcard_id', 'value'=>function($model){ return $model->creditCardName;},'filter'=>\app\models\Creditcard::getList()],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
