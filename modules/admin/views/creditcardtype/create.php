<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Creditcardtype */

$this->title = 'Создание содержание';
$this->params['breadcrumbs'][] = ['label' => 'Creditcardtypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="creditcardtype-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
