<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Promosub */

$this->title = 'Create Promosub';
$this->params['breadcrumbs'][] = ['label' => 'Promosubs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promosub-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
