<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Promosub */

$this->title = 'Редактирование ';
$this->params['breadcrumbs'][] = ['label' => 'Promosubs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="promosub-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
