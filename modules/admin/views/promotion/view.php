<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',


            [
                'attribute' => 'statusMain',
                'filter' => \app\modules\admin\controllers\Label::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\Label::statusLabel($model->statusMain);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'statusRec',
                'filter' => \app\modules\admin\controllers\LabelRec::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\LabelRec::statusLabel($model->statusRec);
                },
                'format' => 'raw',
            ],
            [
                'format' => 'html',
                'attribute' => 'imagea',
                'filter' => '',
                'value' => function($data){
                    return Html::img($data->getImagea(), ['width'=>200]);
                }
            ],
            [
                'format' => 'html',
                'attribute' => 'imageb',
                'filter' => '',
                'value' => function($data){
                    return Html::img($data->getImageb(), ['width'=>200]);
                }
            ],
            'date',
            'type',
            'subcontent',
            'content:ntext',
            'metaName',
            'metaDesc',
            'metaKey',
			'url'


        ],
    ]) ?>

</div>
