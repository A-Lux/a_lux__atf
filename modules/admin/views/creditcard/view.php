<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Creditcard */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Creditcards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="creditcard-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php $fileA = $model->getFileA();?>
    <?php if($fileA != null) $fileA = ['attribute'=>'filea','value' => Html::a('Посмотреть pdf файл',$fileA,['target'=>'_blank']),'format' => 'raw' ];
    else $fileA = "filea"?>

    <?php $fileB = $model->getFileB();?>
    <?php if($fileB != null) $fileB = ['attribute'=>'fileb','value' => Html::a('Посмотреть pdf файл',$fileB,['target'=>'_blank']),'format' => 'raw' ];
    else $fileB = "fileb"?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'format' => 'raw',
                'attribute' => 'nameMob',
                'value' => function($data){
                    return $data->nameMob;
                }
            ],
            [
                'attribute' => 'preOrder',
                'value' => function ($model) {
                    return \app\modules\admin\controllers\Label::statusLabel($model->preOrder);
                },
                'format' => 'raw',
            ],
            [
                'format' => 'html',
                'attribute' => 'image',
                'filter' => '',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>300]);
                }
            ],
			'filenamea',
			'filenameb',
            $fileA,
            $fileB,
            [
                'format' => 'raw',
                'attribute' => 'content',
                'value' => function($data){
                    return $data->content;
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'contentMob',
                'value' => function($data){
                    return $data->contentMob;
                }
            ],


            'url'
        ],
    ]) ?>

</div>
