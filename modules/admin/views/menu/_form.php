<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Menu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menu-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'text')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'status')->dropDownList([0 => 'Скрыто', 1 => 'Доступно']) ?>

        <div class="form-group" >
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
