<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Worktime */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="worktime-form">

    <?php
        $time = array('7'=>'7','8'=>'8','9'=>'9','10'=>'10','11'=>'11','12'=>'12','13'=>'13','14'=>'14',
            '15'=>'15','16'=>'16','17'=>'17','18'=>'18','19'=>'19','20'=>'20','21'=>'21',
            '22'=>'22','23'=>'23','00'=>'00',)
    ?>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'from')->dropDownList($time) ?>

    <?= $form->field($model, 'to')->dropDownList($time) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
