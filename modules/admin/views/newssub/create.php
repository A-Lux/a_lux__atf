<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Newssub */

$this->title = ' Задний фон';
$this->params['breadcrumbs'][] = ['label' => 'Newssubs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newssub-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
