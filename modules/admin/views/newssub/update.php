<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Newssub */

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Newssubs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="newssub-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
