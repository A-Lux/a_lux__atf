<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contactsub */

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Contactsubs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="contactsub-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
