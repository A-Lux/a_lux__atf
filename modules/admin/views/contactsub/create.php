<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contactsub */

$this->title = 'Create Contactsub';
$this->params['breadcrumbs'][] = ['label' => 'Contactsubs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contactsub-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
