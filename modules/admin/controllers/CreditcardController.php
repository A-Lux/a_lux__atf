<?php

namespace app\modules\admin\controllers;

use app\models\FileUpload;
use app\models\ImageUpload;
use Yii;
use app\models\Creditcard;
use app\models\CreditcardSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CreditcardController implements the CRUD actions for Creditcard model.
 */
class CreditcardController extends Controller
{
    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Creditcard models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CreditcardSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Creditcard model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Creditcard model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Creditcard();
        $upload = new ImageUpload();

        if ($model->load(Yii::$app->request->post())) {

            $image = UploadedFile::getInstance($model, 'image');
            $filea = UploadedFile::getInstance($model, 'filea');
            $fileb = UploadedFile::getInstance($model, 'fileb');

            if($image != null){
                $model->image = $upload->uploadFile($image, $model->image);
            }

            if($filea != null) {
                $time = time();
                $filea->saveAs(Yii::getAlias('@web') . 'uploads/pdf/' . $time . '_' . $filea->baseName . '.' . $filea->extension);
                $model->filea = $time . '_' . $filea->baseName . '.' . $filea->extension;
            }

            if($fileb != null) {
                $time = time();
                $fileb->saveAs(Yii::getAlias('@web') . 'uploads/pdf/' . $time . '_' . $fileb->baseName . '.' . $fileb->extension);
                $model->fileb = $time . '_' . $fileb->baseName . '.' . $fileb->extension;
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Creditcard model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $upload = new ImageUpload();
        $oldImage = $model->image;
        $oldFilea = $model->filea;
        $oldFileb = $model->fileb;

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            $filea = UploadedFile::getInstance($model, 'filea');
            $fileb = UploadedFile::getInstance($model, 'fileb');

            if($image == null){
                $model->image = $oldImage;
            }else{
                $model->image = $upload->uploadFile($image, $model->image);
            }

            if($filea == null){
                $model->filea = $oldFilea;
            }else{
                $time = time();
                $filea->saveAs(Yii::getAlias('@web') . 'uploads/pdf/'  . $time . '_' . $filea->baseName . '.' . $filea->extension);
                $model->filea = $time . '_' . $filea->baseName . '.' . $filea->extension;
                  if($oldFilea != null && file_exists(Yii::getAlias('@web') . 'uploads/pdf/'  . $oldFilea)){
                    unlink(Yii::getAlias('@web') . 'uploads/pdf/'  . $oldFilea);
                }
            }

            if($fileb == null){
                $model->fileb = $oldFileb;
            }else{
                $time = time();
                $fileb->saveAs(Yii::getAlias('@web') . 'uploads/pdf/'  . $time . '_' . $fileb->baseName . '.' . $fileb->extension);
                $model->fileb = $time . '_' . $fileb->baseName . '.' . $fileb->extension;
                if($oldFileb != null && file_exists(Yii::getAlias('@web') . 'uploads/pdf/'  . $oldFileb)){
                    unlink(Yii::getAlias('@web') . 'uploads/pdf/'  . $oldFileb);
                }
            }


            if ($model->save(false)) {


                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Creditcard model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Creditcard model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Creditcard the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Creditcard::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
