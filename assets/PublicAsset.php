<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 25.02.2019
 * Time: 17:08
 */

namespace app\assets;

use yii\web\AssetBundle;


class publicAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
            "/public/css/bootstrap.css",
            "/public/css/owl.carousel.min.css",
            "/public/css/owl.theme.default.min.css",
            "/public/css/animate.css",
            "/public/css/multirange.css",
            "/public/css/style.css",
            "/public/css/magnific-popup.css",
		    "/public/css/slick-theme.css",
		    "/public/css/slick.css",
		    "/public/css/partialviewslider.css",
		    "/public/css/partialviewslider.min.css",
		    "/public/css/tingle.min.css",
		    "/public/css/glide.core.min.css",
		    "/public/css/glide.theme.min.css",
            
    ];
    public $js = [
		    
            "/public/js/jquery-3.2.1.min.js",
			"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js",
//		    "https://code.jquery.com/jquery-3.3.1.slim.min.js",
		    "/public/js/partialviewslider.js",
		    "/public/js/partialviewslider.min.js",
            "/public/js/wow.min.js",
            "/public/js/owl.carousel.min.js",
            "/public/js/slick.min.js",
		    "/public/js/slick.js",
		    "/public/js/bootstrap.min.js",
		    "/public/js/flexCarousel.js",
		    "/public/js/tingle.min.js",
		    "/public/js/glide.esm.js",
		    "/public/js/glide.js",
		    "/public/js/glide.min.js",
		    "/public/js/glide.modular.esm.js",
		    "/public/js/main.js",
		    


    ];
    public $depends = [

    ];
}