<?php

namespace app\controllers;


use app\models\Agreement;
use app\models\City;
use app\models\Contact;
use app\models\Contactsub;
use app\models\Creditcard;
use app\models\Creditcardsub;
use app\models\News;
use app\models\Newssub;
use app\models\Page;
use app\models\Promosub;
use app\models\Promotion;
use app\models\Request;
use app\models\Requestsub;
use app\models\Worktime;
use Yii;
use yii\base\Object;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

use yii\filters\VerbFilter;


class SiteController extends FrontendController
{



    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
				  'layout' => 'error',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    protected function sendEmail($name,$phone,$city,$hour, $minute, $day) {

        if($day == 'today' && $hour != null && $minute != null){
            $days = 'Сегодня';
            $emailSend = Yii::$app->mailer->compose()
                ->setFrom('robot@ibeacon.kz')
                ->setTo(Yii::$app->view->params['email']->email)
                ->setSubject('Клиент хочет связаться с вами')

                ->setHtmlBody("<p>Имя: $name</p>
                                 <p>Номер телефона: $phone</p>
                                 <p>Город: $city</p>
                                 <p>Время: $days, $hour:$minute</p>");

        }else if($day == 'tomorrow' && $hour != null && $minute != null){
            try {
                $days = 'Завтра';

            } catch (\Exception $e) {
            }
            $emailSend = Yii::$app->mailer->compose()
                ->setFrom('sdulife.kz@gmail.com')
                ->setTo(Yii::$app->view->params['email']->email)
                ->setSubject('Клиент хочет связаться с вами')

                ->setHtmlBody("<p>Имя: $name</p>
                                 <p>Номер телефона: $phone</p>
                                 <p>Город: $city</p>
                                 <p>Время: $days, $hour:$minute</p>");

        }else{
            $emailSend = Yii::$app->mailer->compose()
                ->setFrom('sdulife.kz@gmail.com')
                ->setTo(Yii::$app->view->params['email']->email)
                ->setSubject('Клиент хочет связаться с вами')

                ->setHtmlBody("<p>Имя: $name</p>
                                 <p>Номер телефона: $phone</p>
                                 <p>Город: $city</p>");
        }


        return $emailSend->send();

    }





    public function actionIndex()
    {
        $model = Page::find()->where('url = "/"')->one();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $card['sub'] = Creditcardsub::find()->one();
        $card['card'] = Creditcard::find()->with('cardItems')->all();
        $news['sub'] = Newssub::find()->one();
        $news['main'] = News::find()->where('statusMain=1')->orderBy('date ASC')->all();
        $promotion['sub'] = Promosub::find()->one();
        $promotion['main'] = Promotion::find()->where('statusMain=1')->orderBy('date ASC')->limit(3)->all();
        $workTime = Worktime::find()->one();
        $request['sub'] = Requestsub::find()->one();
        $contact['sub'] = Contactsub::find()->one();
        $contact['main'] = Contact::find()->all();
        $contact['city'] = City::find()->all();
        $contact['agreement'] = Agreement::find()->one();

        $contact['select']  = ArrayHelper::index($contact['main'], 'city_id');
        $contact['data']  = Contact::getContactWithCity();


        return $this->render('index',compact('card','news','contact','promotion','request','contact','workTime'));
    }






    public function actionCity()
    {

        $model = Contact::find()->where("city_id = ".$_GET['id'])->all();
        $m=0;
        foreach ($model as $v){
            $array = [0 => $v->latitude, 1 => $v->longitude, 2 => $v->address,
                3 => $v->workTime, 4 => $v->telephone];
            $res[$m] = $array;
            $m++;
        }

//        echo $array;
        return json_encode($res);
        die;
    }


    public function actionGetLocationContent()
    {
        $data = Contact::find()->where("city_id =".$_GET['id'])->all();
        return $this->renderAjax('location', compact('data','error'));
    }

    public function actionGetLocationContentCount()
    {
        $data = Contact::find()->where("city_id =".$_GET['id'])->count();
        return $data;
    }


    public function actionGetLocationContentMob()
    {
        $data = Contact::find()->where("city_id =".$_GET['id'])->all();
        return $this->renderAjax('locationMob', compact('data','error'));
    }


    public function actionGetCityName()
    {
        $model = City::find()->where("id = ".$_GET['id'])->one();

        echo $model->name;

        die;
    }




    public function actionNews()
    {
        $model = Page::find()->where('url = "/site/news"')->one();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $news = News::getNews();

        return $this->render('news',compact('news','model'));
    }


    public function actionAds()
    {
        $model = Page::find()->where('url = "/site/news"')->one();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $news = News::getAds();

        return $this->render('news',compact('news','model'));
    }

    public function actionNewsView($id)
    {

        $news = News::findOne($id);
        $model = Page::find()->where('url = "/site/news"')->one();
        $this->setMeta($news->metaName, $news->metaKey, $news->metaDesc);
        $recomend = News::find()->where('statusRec=1 AND id !='.$id)->orderBy('date ASC')->all();
        if($news){

            return $this->render('news-inner',compact('news','recomend','model'));
        }else{
            return $this->render('error');
        }

    }


    public function actionPromotion()
    {
        $model = Page::find()->where('url = "/site/promotion"')->one();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $promotion = Promotion::getAll();

        return $this->render('promotion',compact('promotion','model'));
    }

    public function actionPromotionView($id)
    {
        $promotion = Promotion::findOne($id);
        $model = Page::find()->where('url = "/site/promotion"')->one();
        $this->setMeta($promotion->metaName, $promotion->metaKey, $promotion->metaDesc);
        $recomend = Promotion::find()->where('statusRec=1 AND id !='.$id)->orderBy('date ASC')->all();

        return $this->render('promotion-inner',compact('promotion','model','recomend'));

    }


    public function actionRequest(){

        $model =  new Request();

        $name = $_GET['name'];
        $phone = $_GET['phone'];
        $city = $_GET['city'];
        $hour = $_GET['hour'];
        $minute = $_GET['minute'];
        $day = $_GET['day'];

        if($model->saveRequest($name,$phone,$city,$hour, $minute, $day))
        {
            $array = ['status' => 1];
        }else{
            $array = ['status' => 0];
        }

        return json_encode($array);


    }


    public function actionRequestFromCard(){

        $model =  new Request();

        $name = $_GET['name'];
        $phone = $_GET['phone'];
        $city = $_GET['city'];
        $hour = $_GET['hour'];
        $minute = $_GET['minute'];
        $day = $_GET['day'];
        $card = $_GET['card'];

        if($model->saveRequest($name,$phone,$city,$hour, $minute, $day, $card))
        {
            $array = ['status' => 1];
        }else{
            $array = ['status' => 0];
        }

        return json_encode($array);


    }

    public function actionRequestEmail(){


        $name = $_GET['name'];
        $phone = $_GET['phone'];
        $city = $_GET['city'];
        $hour = $_GET['hour'];
        $minute = $_GET['minute'];
        $day = $_GET['day'];

        if($this->sendEmail($name,$phone,$city,$hour, $minute, $day))
        {
            $array = ['status' => 1];
        }else{
            $array = ['status' => 0];
        }

        return json_encode($array);


    }

}
