<?php
/**
 * Created by PhpStorm.
 * User: Yuriy
 * Date: 22.07.2018
 * Time: 2:02
 */

namespace app\controllers;

use app\models\Banner;
use app\models\Emailforrequest;
use app\models\Logo;
use app\models\Menu;
use app\models\Page;
use app\models\Socialnetwork;
use app\models\Translation;
use Yii;
use yii\web\Controller;

class FrontendController extends Controller
{

    public function init()
    {

        $logo = Logo::find()->one();
        $menu = Menu::find()->orderBy('sort ASC')->all();
        $banner = Banner::find()->one();
        $socialNetwork = Socialnetwork::find()->one();
        $main = Page::find()->one();
        $email = Emailforrequest::find()->one();
        $translation = Translation::find()->all();

        Yii::$app->view->params['menu'] = $menu;
        Yii::$app->view->params['logo'] = $logo;
        Yii::$app->view->params['banner'] = $banner;
        Yii::$app->view->params['socialNetwork'] = $socialNetwork;
        Yii::$app->view->params['main'] = $main;
        Yii::$app->view->params['email'] = $email;
        Yii::$app->view->params['translation'] = $translation;
    }

    protected function setMeta($title = null, $keywords = null, $description = null){
        $this->view->title = $title;
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => "$keywords"]);
        $this->view->registerMetaTag(['name' => 'description', 'content' => "$description"]);
    }

}